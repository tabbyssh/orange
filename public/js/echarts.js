$(function(e) {
	'use strict'
	var month_leads = [];
	var month = [];
	for (var i = 0; i < monthsdata.length; i++) {
  		month_leads[i] = monthsdata[i].month_leads;
  		month[i] = monthsdata[i].month;
	}
	
	/*-----echart7-----*/
	var chartdata4 = [{
		name: 'data',
		type: 'line',
		data: month_leads
	}];
	var option7 = {
		grid: {
			top: '6',
			right: '0',
			bottom: '17',
			left: '25',
		},
		xAxis: {
			data: month,
			axisLine: {
				lineStyle: {
					color: '#F16822'
				}
			},
			axisLabel: {
				fontSize: 10,
				color: '#8492a6  '
			}
		},
		yAxis: {
			splitLine: {
				lineStyle: {
					color: 'rgb(142, 156, 173,0.1)'
				}
			},
			axisLine: {
				lineStyle: {
					color: 'rgb(142, 156, 173,0.1)'
				}
			},
			axisLabel: {
				fontSize: 10,
				color: '#8492a6  '
			}
		},
		series: chartdata4,
		color: ['#F16822 ']
	};
	var chart7 = document.getElementById('echart7');
	var lineChart = echarts.init(chart7);
	lineChart.setOption(option7);
});