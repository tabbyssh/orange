<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/getUserNotifications', 'NotificationsController@getUserNotifications')->name('getUserNotifications');
// Mark Notifications as read
Route::post('/readUserNotification', 'NotificationsController@show')->name('readUserNotification');
// Mark All Notifications as read
Route::post('/readAllUserNotification', 'NotificationsController@showAll')->name('readAllUserNotification');
    
Route::get('/', 'LeadController@create');
Route::post('addlead', 'LeadController@store')->name('addlead');

Route::group([
		'prefix'	=> 'dashboard',
	], function(){
		Route::get('/', 'DashboardController@index');
		Route::resource('lead', 'LeadController');
		Route::get('leadData', 'LeadController@leadData')->name('leadData');
		Route::resource('studio', 'StudioController');
});


Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/home', 'HomeController@index')->name('home');
