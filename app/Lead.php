<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{
    protected $guarded = [];

    /**
     * Get all of the post's comments.
     */
    public function studio()
    {
        return $this->belongsTo('\App\Studio', 'studio_id');
    }

}
