<?php

namespace App\Http\Controllers;

use App\Studio;
use Illuminate\Http\Request;
use App\Http\Requests\StudioStore;

class StudioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('studioListing', ['studios' => Studio::paginate(10)]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StudioStore $request)
    {
        return $request->process();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\studio  $studio
     * @return \Illuminate\Http\Response
     */
    public function show(studio $studio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\studio  $studio
     * @return \Illuminate\Http\Response
     */
    public function edit(studio $studio)
    {
        return $studio;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\studio  $studio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, studio $studio)
    {
        $studio = Studio::find($studio)->first();
        $studio->name = $request->name;
        $studio->email = $request->email;
        $studio->save();
        return $studio;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\studio  $studio
     * @return \Illuminate\Http\Response
     */
    public function destroy(studio $studio)
    {
        $studio = Studio::find($studio)->first();
        $studio->delete();
        return redirect('dashboard/studio/create')->with('error', 'Studio has been deleted');
    }
}
