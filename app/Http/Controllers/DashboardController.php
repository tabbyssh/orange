<?php

namespace App\Http\Controllers;

use App\Lead;
use App\Studio;
use Illuminate\Http\Request;
use DB;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $studioleads = 
        // dd($studioleads);
        $studiolead = Lead::select('studio_id')->groupBy('studio_id')->orderByRaw('COUNT(*) DESC')->limit(1)->with('studio')->first();
        $studioleadsTotal = Lead::where('studio_id', $studiolead->studio_id )->count();
        // dd($studioleadsTotal);
        $monthsdata = DB::table('leads')->select(DB::raw('COUNT(*) as month_leads, MONTHNAME(created_at) as month'))->groupBy(DB::raw('YEAR(created_at) DESC, MONTHNAME(created_at) DESC'))->get();
        return view('dashboard', ['leads' => Lead::with('studio')->orderBy('id', 'desc')->take(5)->get(), 'studios' => Studio::orderBy('id', 'desc')->get(), 'monthsdata' => $monthsdata, 'studiolead'=> $studiolead,'studioleadsTotal'=> $studioleadsTotal ]);
    }
}
