<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NotificationsController extends Controller
{
    public function getUserNotifications(){
    	if(\Auth::check()){
    		$notificationHtml = '';
    		$notificationCount = 0;
        if(count(auth()->user()->unreadNotifications) > 0){
          foreach(auth()->user()->unreadNotifications as $notification){
            $data = $notification->data;
            $notificationHtml.='
                                  <div style="white-space: pre-line;" class="per-notification" data-notification-id="'.$notification->id.'">'.$data[0]['body'].'</div>';
                                  $notificationDate = \Carbon\Carbon::parse($notification->created_at);
                                  $now = \Carbon\Carbon::now();
                                  $diff = $notificationDate->diffInMinutes($now);
                                  if($diff<10):
                                    $notificationHtml.='<div class="small fs-14 text-muted nocount">Just now</div>';
                                  endif;
                                  if($diff>10 && $diff<60):
                                    $notificationHtml.='<div class="small fs-14 text-muted nocount">'.$diff.' minutes ago.</div>';
                                  endif;
                                  if($diff>60 && $diff<1440):
                                    $notificationHtml.='<div class="small fs-14 text-muted nocount">'.$notificationDate->diffInHours($now).' hours ago.</div>';
                                  endif;
                                  if($diff>1440):
                                    $notificationHtml.='<div class="small fs-14 text-muted nocount">'. \Carbon\Carbon::parse($notification->created_at,'UTC')->format('M d, Y').'</div>';
                                  endif;
                $notificationHtml.='';
                $notificationCount++;
          }
        }else{
          $notificationHtml = '<p class="no-notifications">No new notifications.</p>';
        }
	      return response()->json(['status' => "success",'message' => $notificationHtml,'notificationCount'=>$notificationCount], 200);
	    }
	    else{
	        return response()->json(['status' => "error",'message'=>'Please log in to perform this operation'], 500);
	    }
    }
    public function show(Request $request){
    	$notification_id = $request->notification_id;
    	$notification = auth()->user()->notifications()->where('id', $notification_id)->first();
        if ($notification) {
            $notification->markAsRead();
            if(isset($notification->data[0]['actionURL'])){
            	return response()->json(['status' => "success",'redirect_url'=>$notification->data[0]['actionURL']], 200);
            }
        }
    }
    public function showAll(Request $request){
        $notifications = auth()->user()->unreadNotifications;
        foreach ($notifications as $notification) {
            $notification->markAsRead();
        }
        return response()->json(['status' => "success"], 200);
    }
}
