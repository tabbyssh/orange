<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Studio;

class StudioStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'             => 'required',
            'email'                 => ['required', 'email', 'unique:studios'],
        ];
    }

    /**
     * [messages description]
     * @return [type] [description]
     */
    public function messages() {
        return [
            'name.required' => 'The name field is required',
            'email.required' => 'The email field is required',
            'email.unique' => 'Your email has aleready been register',
        ];
    }

    /**
     * Get the data and storing in db.
     *
     * @return array
     */
    public function process()
    {

        return Studio::create([
            'name' => $this->name,
            'email' => $this->email,
        ]);

    }
}
