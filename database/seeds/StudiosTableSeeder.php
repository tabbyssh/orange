<?php

use Illuminate\Database\Seeder;

class StudiosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('studios')->insert([
            [
                'name'	=> 'Addison',
                'email'	=> 'Studio0140@orangetheoryfitness.com',
                'created_at'	=> date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            ],
            [
                'name'  => 'Allen',
                'email' => 'studio0205@orangetheoryfitness.com',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            ],
            [
                'name'  => 'Alliance',
                'email' => 'studio0832@orangetheoryfitness.com',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            ],
            [
                'name'  => 'Arlington Highlands',
                'email' => 'studio0529@orangetheoryfitness.com',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            ],
            [
                'name'  => 'Burleson',
                'email' => 'Studio0963@orangetheoryfitness.com',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            ],
            [
                'name'  => 'Camp Bowie',
                'email' => 'studio0964@orangetheoryfitness.com',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            ],
            [
                'name'  => 'Castle Hills',
                'email' => 'studio0320@orangetheoryfitness.com',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            ],
            [
                'name'  => 'Coppell',
                'email' => 'studio0138@orangetheoryfitness.com',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            ],
            [
                'name'  => 'Flower Mound',
                'email' => 'studio0879@orangetheoryfitness.com',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            ],
            [
                'name'  => 'Frisco',
                'email' => 'studio0130@orangetheoryfitness.com',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            ],
            [
                'name'  => 'Keller',
                'email' => 'studio0244@orangetheoryfitness.com',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            ],
            [
                'name'  => 'Lakewood',
                'email' => 'studio1018@orangetheoryfitness.com',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            ],
            [
                'name'  => 'Las Colinas',
                'email' => 'studio1030@orangetheoryfitness.com',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            ],
            [
                'name'  => 'Lovers',
                'email' => 'studio1423@orangetheoryfitness.com',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            ],
            [
                'name'  => 'Mansfield',
                'email' => 'studio1056@orangetheoryfitness.com',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            ],
            [
                'name'  => 'McKinney',
                'email' => 'studio0338@orangetheoryfitness.com',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            ],
            [
                'name'  => 'Mesquite',
                'email' => 'studio0212@orangetheoryfitness.com',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            ],
            [
                'name'  => 'Montgomery Plaza',
                'email' => 'studio0268@orangetheoryfitness.com',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            ],
            [
                'name'  => 'Murphy',
                'email' => 'studio0530@orangetheoryfitness.com',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            ],
            [
                'name'  => 'Arlington',
                'email' => 'studio1096@orangetheoryfitness.com',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            ],
            [
                'name'  => 'Dallas',
                'email' => 'studio1407@orangetheoryfitness.com',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            ],
            [

                'name'  => 'Plano',
                'email' => 'studio0813@orangetheoryfitness.com',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            ],
            [
                
                'name'  => 'Richardson',
                'email' => 'Studio1274@orangetheoryfitness.com',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            ],
            [
                
                'name'  => 'Preston-Frankford',
                'email' => 'studio0139@orangetheoryfitness.com',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            ],
            [
                
                'name'  => 'Preston Hollow',
                'email' => 'studio0339@orangetheoryfitness.com',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            ],
            [
                
                'name'  => 'Prosper',
                'email' => 'studio0778@orangetheoryfitness.com',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            ],
            [
                
                'name'  => 'Rockwall',
                'email' => 'studio0620@orangetheoryfitness.com',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            ],
            [
                
                'name'  => 'Southlake',
                'email' => 'Studio0132@orangetheoryfitness.com',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            ],
            [
                
                'name'  => 'SW Fort Worth',
                'email' => 'Studio0715@orangetheoryfitness.com',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            ],
            [
                
                'name'  => 'Uptown',
                'email' => 'studio1029@orangetheoryfitness.com',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            ],
            [
                
                'name'  => 'Victory Park',
                'email' => 'Studio1028@orangetheoryfitness.com',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            ],
            [
                
                'name'  => 'Frisco',
                'email' => 'Studio0876@orangetheoryfitness.com',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            ],
            [
                
                'name'  => 'Plano',
                'email' => 'studio0131@orangetheoryfitness.com',
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            ],
        ]);
    }
}
