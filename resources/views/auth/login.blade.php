@extends('layouts.mainapp')

@section('content')
<div  style="height: 969px;padding: 6% 2rem 1rem 2rem; background-image: url('{{ asset('images/media/login_bg.png') }}')" class="main-wrapper" >
    <div class="container">
        <div class="row">
            <div class="col-xl-7 justify-content-center mx-auto text-center">
                <div class="sg-card card">
                    <div class="main-body main-signupbody">
                        <div class="row">
                            <div class="card-body">
                                <div class="col-md-12 col-lg-12 ">
                                    <a href="{{ url('/') }}"><img src="{{ asset('images/brand/logo.png') }}" class="header-brand-img desktop-logo " alt="Dashlot logo"></a>
                                    <h6>Login to Your Account</h6>
                                    <div class="smhr"></div>
                                    <p>Log in to get in the moment updates on the things that interest you.</p>
                                    <form method="POST" action="{{ route('login') }}">
                                        @csrf
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input placeholder="Email Address" id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                                    @error('email')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input placeholder="Password" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                                        @error('password')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-4 ">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                                    <label class="form-check-label" for="remember">
                                                        {{ __('Remember Me') }}
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-8 ">
                                                
                                                @if (Route::has('password.request'))
                                                    <a class="pwlink btn btn-link" href="{{ route('password.request') }}">
                                                        {{ __('Forgot Your Password?') }}
                                                    </a>
                                                @endif
                                            </div>

                                        </div>

                                        <div class="form-group row mb-0">
                                            <div class="col-md-12 col-lg-12">
                                                <button type="submit" class="msubb btn btn-primary">
                                                    {{ __('Login') }}
                                                    <i class="fa fa-arrow-right"></i>
                                                </button>
                                            </div>  
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('title')
Login
@endsection