<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Fav Icon -->
    <link rel="icon" href="{{ asset('images/brand/favicon.png') }}" type="image/gif" sizes="16x16">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Bootstrap css -->
    <link href="{{ asset('plugins/bootstrap-4.1.3/css/bootstrap.min.css') }}" rel="stylesheet" />

    <!-- Style css -->
    <link  href="{{ asset('css/style.css') }}" rel="stylesheet" />

    <!-- Default css -->
    <link href="{{ asset('css/default.css') }}" rel="stylesheet">

    <!-- Owl-carousel css-->
    <link href="{{ asset('plugins/owl-carousel/owl.carousel.css') }}" rel="stylesheet" />

    <!-- Bootstrap-daterangepicker css -->
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">

    <!-- Bootstrap-datepicker css -->
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/bootstrap-datepicker.css') }}">

    <!-- Custom scroll bar css -->
    <link href="{{ asset('plugins/scroll-bar/jquery.mCustomScrollbar.css') }}" rel="stylesheet"/>

    <!-- Horizontal css -->
    <link id="effect" href="{{ asset('plugins/horizontal-menu/dropdown-effects/fade-up.css') }}" rel="stylesheet" />
    <link href="{{ asset('plugins/horizontal-menu/horizontal.css') }}" rel="stylesheet" />

    <!-- Data table css -->
    <link href="{{ asset('plugins/datatable/dataTables.bootstrap4.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('plugins/datatable/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('plugins/datatable/css/buttons.bootstrap4.min.css') }}">
    <link href="{{ asset('plugins/datatable/responsive.bootstrap4.min.css') }}" rel="stylesheet" />

    <!-- P-scroll css -->
    <link href="{{ asset('plugins/p-scroll/p-scroll.css') }}" rel="stylesheet" type="text/css">

    <!-- Font-icons css -->
    <link  href="{{ asset('css/icons.css') }}" rel="stylesheet">

    <!-- Rightsidebar css -->
    <link href="{{ asset('plugins/sidebar/sidebar.css') }}" rel="stylesheet">

    <!-- Nice-select css  -->
    <link href="{{ asset('plugins/jquery-nice-select/css/nice-select.css') }}" rel="stylesheet"/>

    <!-- Color-palette css-->
    <link rel="stylesheet" href="{{ asset('css/skins.css') }}"/>

    {{-- Toastr Css --}}
    <link href="{{ asset('toastr/build/toastr.css') }}" rel="stylesheet" type="text/css" />

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <!-- Loader -->
    <div id="loading">
        <img src="{{ asset('images/other/loader.svg') }}" class="loader-img" alt="Loader">
    </div>
    <!-- PAGE -->
        <div class="page">
            <div class="page-main">

                <!-- Top-header opened -->
                <div class="header-main header sticky">
                    <div class="app-header header top-header navbar-collapse ">
                        <div class="container">
                            <a id="horizontal-navtoggle" class="animated-arrow hor-toggle"><span></span></a><!-- sidebar-toggle-->
                            <div class="d-flex">
                                <a class="header-brand" href="{{ url('/') }}">
                                    <img style="height: 60px;" src="{{ asset('images/brand/logo.png') }}" class="header-brand-img desktop-logo " alt="Orange Theory logo">
                                    <img style="height: 60px;" src="{{ asset('images/brand/logo.png') }}" class="header-brand-img desktop-logo-1 " alt="Orange Theory logo">
                                    <img src="{{ asset('images/brand/favicon.png') }}" class="mobile-logo" alt="Orange Theory logo">
                                    <img src="{{ asset('images/brand/favicon.png') }}" class="mobile-logo-1" alt="Orange Theory logo">
                                </a>
                                <div class="d-flex header-right ml-auto">
                                    <div class="dropdown header-notify">
                                        <a class="nav-link icon text-center show-notifications" data-toggle="dropdown">
                                            <i class="typcn typcn-bell bell-animations"></i>
                                            <span class="pulse bg-success"></span>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right animated bounceInDown dropdown-menu-arrow w-250">
                                            <div class="dropdown-header p-4 mb-2 bg-header-image p-5 text-white">
                                                <h5 class="dropdown-title mb-1 font-weight-semibold text-drak">Notifications</h5>
                                                <p class="dropdown-title-text subtext mb-0 pb-0 fs-13">You have {{ count(auth()->user()->unreadNotifications) }} new notifications</p>
                                            </div>
                                            <div class="drop-notify">
                                                <a href="#" class="dropdown-item d-flex pb-3 pl-4 pr-2 border-bottom notificationa">
                                                    <div class="notifyimg bg-info-transparent text-info-shadow">
                                                        <i class="fa fa-envelope-o fs-18 text-info"></i>
                                                    </div>
                                                    <div>
                                                        <strong>
                                                            3 new Comments
                                                        </strong>
                                                        <div class="small fs-14 text-muted">
                                                            5 days ago
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="dropdown-divider mb-0"></div>
                                            {{-- <a href="#" class="dropdown-item text-center br-br-6 br-bl-6">See all Messages</a> --}}
                                        </div>
                                    </div><!-- Notification -->
                                    <div class="dropdown drop-profile">
                                        <a class="nav-link pr-0 leading-none" href="#" data-toggle="dropdown" aria-expanded="false">
                                            <div class="profile-details mt-1">
                                                <span class="mr-3 mb-0  fs-15 font-weight-semibold">Milton Salazar</span>
                                                <small class="text-muted mr-3">Executive Sales</small>
                                            </div>
                                            <img class="avatar avatar-md brround" src="{{ asset('images/users/2.png') }}" alt="image">
                                         </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow animated bounceInDown w-250">
                                            <div class="user-profile bg-header-image border-bottom p-3">
                                                <div class="user-image text-center">
                                                    <img class="user-images" src="{{ asset('images/users/2.png') }}" alt="image">
                                                </div>
                                                <div class="user-details text-center">
                                                    <h4 class="mb-0">Milton Salazar</h4>
                                                    <p class="mb-1 fs-13 text-white-50">Executive Sales</p>
                                                </div>
                                            </div>
                                            {{-- <a class="dropdown-item" href="#">
                                                <i class="dropdown-icon mdi mdi-account-outline "></i> Profile
                                            </a>
                                            <a class="dropdown-item" href="#">
                                                <i class="dropdown-icon  mdi mdi-settings"></i> Settings
                                            </a>
                                            <a class="dropdown-item" href="#">
                                                <span class="float-right"><span class="badge badge-success">6</span></span>
                                                <i class="dropdown-icon mdi  mdi-message-outline"></i> Inbox
                                            </a>
                                            <a class="dropdown-item" href="#">
                                                <i class="dropdown-icon mdi mdi-comment-check-outline"></i> Message
                                            </a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="#">
                                                <i class="dropdown-icon mdi mdi-compass"></i> Need help?
                                            </a> --}}
                                            <a class="dropdown-item mb-1" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                <i class="dropdown-icon mdi  mdi-logout-variant"></i> Sign out
                                            </a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                        </div>
                                    </div><!-- Profile -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Top-header closed -->

                <!-- Horizontal-menu -->
                <div class="horizontal-main hor-menu clearfix">
                    <div class="horizontal-mainwrapper container clearfix">
                        <nav class="horizontalMenu clearfix">
                            <ul class="horizontalMenu-list">
                                <li aria-haspopup="true"><a href="{{ url('dashboard') }}" class="sub-icon "><svg xmlns="http://www.w3.org/2000/svg"  class="icon_img" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 365.001 365.001" xml:space="preserve"><g><g>
                                        <g>
                                            <path d="M360.74,155.711l-170-149c-4.717-4.133-11.764-4.133-16.48,0l-170,149c-5.191,4.55-5.711,12.448-1.161,17.641    c4.55,5.19,12.449,5.711,17.64,1.16l13.163-11.536V348.89c0,6.903,5.596,12.5,12.5,12.5h94.733h82.73h94.732   c6.904,0,12.5-5.597,12.5-12.5V162.977l13.163,11.537c2.373,2.078,5.311,3.1,8.234,3.1c3.476,0,6.934-1.441,9.405-4.261    C366.452,168.159,365.932,160.262,360.74,155.711z M153.635,336.39V233.418h57.729v102.973L153.635,336.39L153.635,336.39z     M306.099,141.161V336.39h-69.732V220.918c0-6.903-5.598-12.5-12.5-12.5h-82.73c-6.903,0-12.5,5.597-12.5,12.5v115.473H58.903    V141.161c0-0.032-0.004-0.062-0.004-0.093L182.5,32.733l123.603,108.334C306.104,141.1,306.099,141.129,306.099,141.161z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
                                            <path d="M154.5,120.738c0,6.904,5.596,12.5,12.5,12.5h31c6.903,0,12.5-5.596,12.5-12.5s-5.597-12.5-12.5-12.5h-31    C160.097,108.238,154.5,113.834,154.5,120.738z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
                                        </g>
                                    </g></g> </svg> Dashboard </a>
                                </li>
                                <li aria-haspopup="true"><a href="{{ route('lead.index') }}" class="sub-icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" xml:space="preserve" class="icon_img"><g><g>
                                        <g>
                                            <g>
                                                <path d="M352.459,220c0-11.046-8.954-20-20-20h-206c-11.046,0-20,8.954-20,20s8.954,20,20,20h206     C343.505,240,352.459,231.046,352.459,220z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
                                                <path d="M126.459,280c-11.046,0-20,8.954-20,20c0,11.046,8.954,20,20,20H251.57c11.046,0,20-8.954,20-20c0-11.046-8.954-20-20-20     H126.459z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
                                                <path d="M173.459,472H106.57c-22.056,0-40-17.944-40-40V80c0-22.056,17.944-40,40-40h245.889c22.056,0,40,17.944,40,40v123     c0,11.046,8.954,20,20,20c11.046,0,20-8.954,20-20V80c0-44.112-35.888-80-80-80H106.57c-44.112,0-80,35.888-80,80v352     c0,44.112,35.888,80,80,80h66.889c11.046,0,20-8.954,20-20C193.459,480.954,184.505,472,173.459,472z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
                                                <path d="M467.884,289.572c-23.394-23.394-61.458-23.395-84.837-0.016l-109.803,109.56c-2.332,2.327-4.052,5.193-5.01,8.345     l-23.913,78.725c-2.12,6.98-0.273,14.559,4.821,19.78c3.816,3.911,9,6.034,14.317,6.034c1.779,0,3.575-0.238,5.338-0.727     l80.725-22.361c3.322-0.92,6.35-2.683,8.79-5.119l109.573-109.367C491.279,351.032,491.279,312.968,467.884,289.572z      M333.776,451.768l-40.612,11.25l11.885-39.129l74.089-73.925l28.29,28.29L333.776,451.768z M439.615,346.13l-3.875,3.867     l-28.285-28.285l3.862-3.854c7.798-7.798,20.486-7.798,28.284,0C447.399,325.656,447.399,338.344,439.615,346.13z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
                                                <path d="M332.459,120h-206c-11.046,0-20,8.954-20,20s8.954,20,20,20h206c11.046,0,20-8.954,20-20S343.505,120,332.459,120z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
                                            </g>
                                        </g>
                                    </g></g> </svg>
                                    Lead Form</a>

                                    <li aria-haspopup="true"><a href="{{ route('studio.create') }}" class="sub-icon">
                                        <img style="width: 18px;" src="{{ asset('images/media/icon-nav.png') }}">
                                    Studio</a>
                                
                            </ul>
                        </nav>
                        <!--Nav end -->
                    </div>
                </div>
                <!-- Horizontal-menu end -->

                <!-- App-content opened -->
                <div class="app-content">
                    <div class="container">
                        @yield('content')
                    </div>
                </div>
            </div>

            <footer class="footer-main">
                <div class="container">
                    <div class="  mt-2 mb-2 text-center">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mkleft">
                                    Copyright © 2019 Orange Theory Fitness.
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="makeright">
                                    The Barber Shop  |  Marketing and Promotions
                                <div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>

        </div>
    <!-- Jquery-scripts -->
    <script src="{{ asset('js/vendors/jquery-3.2.1.min.js') }}"></script>

    <!-- Moment js-->
    <script src="{{ asset('plugins/moment/moment.min.js') }}"></script>

    <!-- Bootstrap-scripts js -->
    <script src="{{ asset('js/vendors/bootstrap.bundle.min.js') }}"></script>

    <!-- Sparkline JS-->
    <script src="{{ asset('js/vendors/jquery.sparkline.min.js') }}"></script>

    <!-- Bootstrap-daterangepicker js -->
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

    <!-- Bootstrap-datepicker js -->
    <script src="{{ asset('plugins/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>

    <!-- Chart-circle js -->
    <script src="{{ asset('js/vendors/circle-progress.min.js') }}"></script>

    <!-- Rating-star js -->
    <script src="{{ asset('plugins/rating/jquery.rating-stars.js') }}"></script>

    <!-- Clipboard js -->
    <script src="{{ asset('plugins/clipboard/clipboard.min.js') }}"></script>
    <script src="{{ asset('plugins/clipboard/clipboard.js') }}"></script>

    <!-- Prism js -->
    <script src="{{ asset('plugins/prism/prism.js') }}"></script>

    <!-- Custom scroll bar js-->
    <script src="{{ asset('plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js') }}"></script>

    <!-- Nice-select js-->
    <script src="{{ asset('plugins/jquery-nice-select/js/jquery.nice-select.js') }}"></script>
    <script src="{{ asset('plugins/jquery-nice-select/js/nice-select.js') }}"></script>

    <!-- P-scroll js -->
    <script src="{{ asset('plugins/p-scroll/p-scroll.js') }}"></script>
    <script src="{{ asset('plugins/p-scroll/p-scroll-horizontal.js') }}"></script>

    <!-- JQVMap -->
    <script src="{{ asset('plugins/jqvmap/jquery.vmap.js') }}"></script>
    <script src="{{ asset('plugins/jqvmap/maps/jquery.vmap.world.js') }}"></script>
    <script src="{{ asset('plugins/jqvmap/jquery.vmap.sampledata.js') }}"></script>
    <!-- Sidemenu js-->
    <script src="{{ asset('plugins/sidemenu/sidemenu.js') }}"></script>
    <!-- Apexchart js-->
    <script src="{{ asset('js/apexcharts.js') }}"></script>

    <!-- Chart js-->
    <script src="{{ asset('plugins/chart/chart.min.js') }}"></script>

    <!-- Index js -->
    <script src="{{ asset('js/index.js') }}"></script>
    <script src="{{ asset('js/index-map.js') }}"></script>

    <script src="{{ asset('toastr/build/toastr.min.js') }}" type="text/javascript"></script>

    <!-- Data tables -->
    <script src="{{ asset('plugins/datatable/js/jquery.dataTables.j') }}s"></script>
    <script src="{{ asset('plugins/datatable/js/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('plugins/datatable/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/datatable/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatable/js/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/datatable/js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/datatable/js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/datatable/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/datatable/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('plugins/datatable/js/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('plugins/datatable/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatable/responsive.bootstrap4.min.js') }}"></script>

    <!-- Data table js -->
    <script src="{{ asset('js/datatable.js') }}"></script>

    <!-- Horizontal js-->
    <script src="{{ asset('plugins/horizontal-menu/horizontal.js') }}"></script>

    <!-- Rightsidebar js -->
    <script src="{{ asset('plugins/sidebar/sidebar.js') }}"></script>

    <!-- Echart js -->
    <script src="{{ asset('plugins/echarts/echarts.js') }}"></script>
    <script src="{{ asset('js/echarts.js') }}"></script>

    <!-- Echart Param -->
    <script>var monthsdata = {!! $monthsdata ?? '' !!}</script>
    
    <!-- Custom js -->
    <script src="{{ asset('js/custom.js') }}"></script>
    @stack('scripts')
    <script>
        @if(session('success'))
            toastr.success('{{ session('success') }}');
        @endif
        @if(session('error'))
            toastr.error('{{ session('error') }}');
        @endif
    </script>
    <script type="text/javascript">
        $(".show-notifications").click(function(event) {
            $(".notificationa").html("<span class='loading'> Loading <i class='fa fa-spin fa-spinner'></i></span>");
            $.ajax({
                url: "{{ route('getUserNotifications') }}",
                type: 'GET',
                dataType: 'json',
                async :     true,
                headers: {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
                beforeSend: function( xhdr, data, statusText){},
                success: function( data, statusText, xhdr ){
                    $(".notificationa").html('<div class="notifyimg bg-info-transparent text-info-shadow"><i class="fa fa-envelope-o fs-18 text-info"></i></div><div><div class="nomeesgae">3 new Comments</div></div>');
                    $(".nomeesgae").html(data.message);
                },
                error: function( data, statusText, xhdr ){
                    toastr.error(data.message);
                },
            });
        });
        $('body').on('click touchstart','.notificationa .per-notification',function(event) {
            var notification_id = $(this).attr('data-notification-id');
            $.ajax({
              url: "{{ route('readUserNotification') }}",
              type: 'POST',
              dataType: 'json',
              async :   true,
              data: {notification_id:notification_id},
              headers: {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
              beforeSend: function( xhdr, data, statusText){},
              success: function( data, statusText, xhdr ){
                window.location.href = data.redirect_url;
              },
              error: function( data, statusText, xhdr ){
                location.reload();
              },
            });
        });
    </script>
</body>
</html>
