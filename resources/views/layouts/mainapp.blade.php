<!doctype html>
<html style="" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head style="min-height: 100%;
    height: 100%;height: 100%;
    ">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>
    
    <!-- Fav Icon -->
    <link rel="icon" href="{{ asset('images/brand/favicon.png') }}" type="image/gif" sizes="16x16">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Bootstrap css -->
    <link href="{{ asset('plugins/bootstrap-4.1.3/css/bootstrap.min.css') }}" rel="stylesheet" />

    <!-- Style css -->
    <link  href="{{ asset('css/style.css') }}" rel="stylesheet" />

    <!-- Default css -->
    <link href="{{ asset('css/default.css') }}" rel="stylesheet">

    <!-- Owl-carousel css-->
    <link href="{{ asset('plugins/owl-carousel/owl.carousel.css') }}" rel="stylesheet" />

    <!-- Bootstrap-daterangepicker css -->
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">

    <!-- Bootstrap-datepicker css -->
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/bootstrap-datepicker.css') }}">

    <!-- Custom scroll bar css -->
    <link href="{{ asset('plugins/scroll-bar/jquery.mCustomScrollbar.css') }}" rel="stylesheet"/>

    <!-- Horizontal css -->
    <link id="effect" href="{{ asset('plugins/horizontal-menu/dropdown-effects/fade-up.css') }}" rel="stylesheet" />
    <link href="{{ asset('plugins/horizontal-menu/horizontal.css') }}" rel="stylesheet" />

    <!-- P-scroll css -->
    <link href="{{ asset('plugins/p-scroll/p-scroll.css') }}" rel="stylesheet" type="text/css">

    <!-- Font-icons css -->
    <link  href="{{ asset('css/icons.css') }}" rel="stylesheet">

    <!-- Rightsidebar css -->
    <link href="{{ asset('plugins/sidebar/sidebar.css') }}" rel="stylesheet">

    <!-- Nice-select css  -->
    <link href="{{ asset('plugins/jquery-nice-select/css/nice-select.css') }}" rel="stylesheet"/>

    <!-- Color-palette css-->
    <link rel="stylesheet" href="{{ asset('css/skins.css') }}"/>

    {{-- Toastr Css --}}
    <link href="{{ asset('toastr/build/toastr.css') }}" rel="stylesheet" type="text/css" />

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="mainApp">
    <!-- Loader -->
    <div id="loading">
        <img src="{{ asset('images/other/loader.svg') }}" class="loader-img" alt="Loader">
    </div>
    <!-- PAGE -->
        <div class="page" style="min-height: 100%; height: 100%;">
            <div class="page-main">
                @yield('content')
        </div>
    </div>
    <!-- Jquery-scripts -->
    <script src="{{ asset('js/vendors/jquery-3.2.1.min.js') }}"></script>

    <!-- Moment js-->
    <script src="{{ asset('plugins/moment/moment.min.js') }}"></script>

    <!-- Bootstrap-scripts js -->
    <script src="{{ asset('js/vendors/bootstrap.bundle.min.js') }}"></script>

    <!-- Sparkline JS-->
    <script src="{{ asset('js/vendors/jquery.sparkline.min.js') }}"></script>

    <!-- Bootstrap-daterangepicker js -->
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

    <!-- Bootstrap-datepicker js -->
    <script src="{{ asset('plugins/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>

    <!-- Chart-circle js -->
    <script src="{{ asset('js/vendors/circle-progress.min.js') }}"></script>

    <!-- Rating-star js -->
    <script src="{{ asset('plugins/rating/jquery.rating-stars.js') }}"></script>

    <!-- Clipboard js -->
    <script src="{{ asset('plugins/clipboard/clipboard.min.js') }}"></script>
    <script src="{{ asset('plugins/clipboard/clipboard.js') }}"></script>

    <!-- Prism js -->
    <script src="{{ asset('plugins/prism/prism.js') }}"></script>

    <!-- Custom scroll bar js-->
    <script src="{{ asset('plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js') }}"></script>

    <!-- Nice-select js-->
    <script src="{{ asset('plugins/jquery-nice-select/js/jquery.nice-select.js') }}"></script>
    <script src="{{ asset('plugins/jquery-nice-select/js/nice-select.js') }}"></script>

    <!-- P-scroll js -->
    <script src="{{ asset('plugins/p-scroll/p-scroll.js') }}"></script>
    <script src="{{ asset('plugins/p-scroll/p-scroll-horizontal.js') }}"></script>

    <!-- JQVMap -->
    <script src="{{ asset('plugins/jqvmap/jquery.vmap.js') }}"></script>
    <script src="{{ asset('plugins/jqvmap/maps/jquery.vmap.world.js') }}"></script>
    <script src="{{ asset('plugins/jqvmap/jquery.vmap.sampledata.js') }}"></script>

    <!-- Apexchart js-->
    <script src="{{ asset('js/apexcharts.js') }}"></script>

    <!-- Chart js-->
    <script src="{{ asset('plugins/chart/chart.min.js') }}"></script>

    <!-- Index js -->
    <script src="{{ asset('js/index.js') }}"></script>
    <script src="{{ asset('js/index-map.js') }}"></script>

    <script src="{{ asset('toastr/build/toastr.min.js') }}" type="text/javascript"></script>
    <!-- Horizontal js-->
    <script src="{{ asset('plugins/horizontal-menu/horizontal.js') }}"></script>

    <!-- Rightsidebar js -->
    <script src="{{ asset('plugins/sidebar/sidebar.js') }}"></script>

    <!-- Custom js -->
    <script src="{{ asset('js/custom.js') }}"></script>
    @stack('scripts')
</body>
</html>
