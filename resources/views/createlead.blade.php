@extends('layouts.mainapp')

@section('content')
    <div  style="background-size: contain;min-height: 100%;height: 100%;padding: 6% 2rem 1rem 2rem; background-image: url('{{ asset('images/media/signup_bg.png') }}')" class="main-wrapper" >
        <div class="container">
            <div class="row">
                <div class="col-xl-7 justify-content-center mx-auto text-center">
                    <div class="sg-card card">
                        <div class="main-body main-signupbody">
                            <div class="row">
                                <div class="card-body about-con pabout">
                                    <div class="col-md-12 col-lg-12 ">
                                        <a href="{{ url('/') }}"><img src="{{ asset('images/brand/logo.png') }}" class="header-brand-img desktop-logo " alt="Dashlot logo"></a>
                                        <div class="smhr"></div>
                                        <h6>I’m ready to Test the Theory</h6>
                                    	<form id="leadform">
                                    		@csrf
                                    		<div class="row">
                                    			<div class="col-md-6">
                                    				<div class="form-group">
                                    					<input type="text" class="form-control" name="first_name" placeholder="First Name">
                                    				</div>
                                    			</div>
                                    			<div class="col-md-6">
                                    				<div class="form-group">
                                    					<input type="text" class="form-control" name="last_name" placeholder="Last Name">
                                    				</div>
                                    			</div>
                                    		</div>
                                    		<div class="row">
                                    			<div class="col-md-12">
                                    				<div class="form-group">
                                    					<input type="text" class="form-control" name="number" placeholder="Phone Number">
                                    				</div>
                                    			</div>
                                    		</div>
                                    		<div class="row">
                                    			<div class="col-md-12">
                                    				<div class="form-group">
                                    					<input type="email" class="form-control" name="email" placeholder="Email">
                                    				</div>
                                    			</div>
                                    		</div>
                                    		<div class="row">
                                    			<div class="col-md-12">
                                                    <div class="std-slc" style="text-align: left;">
                                                        <label style="display: inline-block;width: 100%;" class="std-sc">Studio Selection</label>
                                                    </div>
                                                    <div class="radio_series row">
                                    			    	@foreach($studios as $studio)
                                                        <div class="col-md-4 col-lg-4">
                                        			    	<label class="custom-control custom-radio ">
                                        							<input type="radio" class="custom-control-input" name="studio[]" value="{{ $studio->id }}" >
                                        							<span class="custom-control-label">{{ $studio->name }}</span>
                                        					</label>
                                                        </div>
                                    			        @endforeach
                                                    </div>
                                			    </div>
                                	        </div>
                                	        <div class="row">
                                    			<div class="col-md-12">
                                					<div class="mt-2 mb-2">
                                						<button id="form-btn" type="submit" class="msubb btn btn-primary btn-lg">Submit Form <i class="fa fa-arrow-right"></i></button>
                                					</div>
                                				</div>
                                			</div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('title')
Sign Up
@endsection
@push('scripts')
	<script type="text/javascript">
		$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
		$('#form-btn').on('click', function() {

			event.preventDefault();

		 	$('#form-btn').prop("disabled", true);
            $('#form-btn').html("Loading <i class='fa fa-spin fa-spinner'></i>");
            var formData = new FormData($('#leadform')[0]);
            $.ajax({
                url: '{{ route('addlead') }}',
                type: 'POST',
                dataType: 'JSON',
                data: formData,
                contentType: false,
                cache: false,
                processData: false,
                success:function(data){
                    $('#leadform').trigger('reset');
                    $('#form-btn').html("Submit");
                    $('#form-btn').removeAttr("disabled");
                    toastr.success('Request has been submitted');
                    setTimeout(function(){
                       window.location.reload(1);
                    }, 3000);
                },
                error:function(data, statusText, xhdr){
                    var errors = data.responseJSON.errors;
                    var message = '';
                    $.each(errors,function(index, val) {
                        message += val[0]+'<br />';
                    });
                    toastr.error(message);
                    $('#form-btn').html("Submit");
                    $('#form-btn').removeAttr("disabled");
                }
            });
		});
	</script>
@endpush
{{-- <lead-from studios="{{ $studios }}"></lead-from> --}}