@extends('layouts.dashboardLayout')

@section('content')


<!-- row opened -->
<div class="page-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#"><img style="width: 18px;" src="{{ asset('images/media/icon-nav-black.png') }}"> Studio</a></li>
    </ol>
    <div class="mt-3 mt-lg-0">
        <div class="d-flex align-items-center flex-wrap text-nowrap">
            <button data-toggle="modal" data-target="#addStudioModal" type="button" class="btn btn-primary btn-icon-text mb-2 mb-md-0">
                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                Add Studio
            </button>
        </div>
    </div>
</div>

<!-- row opened -->
<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="card">
            <div class="">
                <div class="table-responsive">
                    <div class="dataTables_wrapper studioTableWarapper">
                        <table class="studioListingTable table table-bordered border-top table-hover mb-0 text-nowrap">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Studio Name</th>
                                    <th>Email Address</th>
                                    <th style="text-align: center;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($studios) > 0)
                                    @foreach($studios as $studio)
                                    <tr>
                                        <th style="text-align: center;" scope="row">{{ $studio->id }}</th>
                                        <td>{{ $studio->name }}</td>
                                        <td>{{ $studio->email }}</td>
                                        <td align="center"><button data-edit-action="{{ route('studio.edit', $studio->id ) }}" data-update-action="{{ route('studio.update', $studio->id ) }}" type="button" id="edit-btn" class="editbtn btn btn-primary btn-icon-text mb-2 mb-md-0" data-toggle="modal" data-target="#editStudioModal" >Edit</button>
                                            <form style="display: inline-block;" action="{{route('studio.destroy', $studio->id ) }}" method="POST">
                                                @method('delete')
                                                @csrf
                                                <button type="submit"  id="delete-btn" class="delete-btn btn btn-primary btn-icon-text mb-2 mb-md-0"><i class="fa fa-trash"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                        <div class="dataTables_paginate paging_simple_numbers" id="myDataTable_paginate">
                            {{ $studios->links() }}
                        </div>
                    </div>
                </div>
            </div>
            <!-- table-wrapper -->
        </div>
        <!-- section-wrapper -->
    </div>
</div>
<!-- row closed -->

<div class="modal fade addStudioModal" id="addStudioModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                {{-- <h5 class="modal-title" id="example-Modal3">New message</h5> --}}
                <img src="{{ asset('images/brand/logo.jpg') }}" class="header-brand-img desktop-logo " alt="Dashlot logo">
                <button type="button" class="close " data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="createStudioForm">
                    @csrf
                    <div class="form-group">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Studio Name">
                    </div>
                    <div class="">
                        <input type="text" class="form-control" id="email" name="email" placeholder="Email">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="footerbtn">
                    <button type="button" id="addStudioBtn" class="addStudioBtn btn btn-primary">Save Changes <i class="fa fa-arrow-right"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade editStudioModal" id="editStudioModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                {{-- <h5 class="modal-title" id="example-Modal3">New message</h5> --}}
                <img src="{{ asset('images/brand/logo.jpg') }}" class="header-brand-img desktop-logo " alt="Dashlot logo">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="updateStudioForm">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Studio Name">
                    </div>
                    <div class="">
                        <input type="text" class="form-control" id="email" name="email" placeholder="Email">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="footerbtn">
                    <button data-update-action="" type="button" id="UpdateStudioBtn" class="UpdateStudioBtn btn btn-primary">Update Changes <i class="fa fa-arrow-right"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('title')
Studio Listing
@endsection
@push('scripts')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#addStudioBtn').on('click', function() {
            event.preventDefault();

            $('#addStudioBtn').prop("disabled", true);
            $('#addStudioBtn').html("Loading <i class='fa fa-spin fa-spinner'></i>");
            var formData = new FormData($('#createStudioForm')[0]);
            $.ajax({
                url: '{{ route('studio.store') }}',
                type: 'POST',
                dataType: 'JSON',
                data: formData,
                contentType: false,
                cache: false,
                processData: false,
                success:function(data){
                    $('#createStudioForm').trigger('reset');
                    $('#addStudioBtn').html("Submit");
                    $('#addStudioBtn').removeAttr("disabled");
                    $('#addStudioModal').modal('hide');
                    toastr.success('Request has been submitted');
                    setTimeout(function(){
                       window.location.reload(1);
                    }, 2000);
                },
                error:function(data, statusText, xhdr){
                    var errors = data.responseJSON.errors;
                    var message = '';
                    $.each(errors,function(index, val) {
                        message += val[0]+'<br />';
                    });
                    toastr.error(message);
                    $('#addStudioBtn').html("Submit");
                    $('#addStudioBtn').removeAttr("disabled");
                }
            });
        });
    </script>
    <script type="text/javascript">
        $('.editbtn').on('click', function(){
            var editAction = $(this).data('edit-action');
            var updateAction = $(this).data('update-action');
            $.ajax({
                url: editAction,
                type: 'GET',
                dataType: 'JSON',
                success:function(data){
                    $('.editStudioModal #name').val(data.name);
                    $('.editStudioModal #email').val(data.email);
                    $('.editStudioModal #UpdateStudioBtn').attr('data-update-action', updateAction);
                    // console.log(data);
                },
                error:function(data, statusText, xhdr){
                    var errors = data.responseJSON.errors;
                    var message = '';
                    $.each(errors,function(index, val) {
                        message += val[0]+'<br />';
                    });
                    toastr.error(message);
                    $('#editbtn').html("Submit");
                    $('#editbtn').removeAttr("disabled");
                }
            });
        });
        $('#UpdateStudioBtn').on('click', function(){
            $('#UpdateStudioBtn').prop("disabled", true);
            $('#UpdateStudioBtn').html("Loading <i class='fa fa-spin fa-spinner'></i>");
            var updateAction = $(this).data('update-action');
            var formData = new FormData($('#updateStudioForm')[0]);
            $.ajax({
                url: updateAction,
                type: 'POST',
                data: formData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,
                success:function(data){
                    $('#UpdateStudioBtn').html("Submit");
                    $('#UpdateStudioBtn').removeAttr("disabled");
                    $('#addStudioModal').modal('hide');
                    toastr.success('Request has been updated');
                    setTimeout(function(){
                       window.location.reload(1);
                    }, 2000);
                },
                error:function(data, statusText, xhdr){
                    var errors = data.responseJSON.errors;
                    var message = '';
                    $.each(errors,function(index, val) {
                        message += val[0]+'<br />';
                    });
                    toastr.error(message);
                    $('#UpdateStudioBtn').html("Submit");
                    $('#UpdateStudioBtn').removeAttr("disabled");
                }
            });
        });
    </script>
@endpush
