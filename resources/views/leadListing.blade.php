@extends('layouts.dashboardLayout')

@section('content')


<!-- row opened -->
<div class="page-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#"><i class="ti-package mr-1"></i> Lead Form</a></li>
    </ol>
    <div class="mt-3 mt-lg-0">
        <div class="d-flex align-items-center flex-wrap text-nowrap">
            <button id="exportbtn" style="background: #CA373D;    border-color: rgba(249, 72, 89, 0.3); box-shadow: 0 5px 10px rgba(249, 72, 89, 0.3) !important;" type="button" class="btn btn-success btn-icon-text mr-2 d-none d-md-block">
                <i class="fe fe-download"></i>
                Export
            </button>
            <button id="printbtn" style="background-color: #36b37e; border-color: #36b37e; box-shadow: 0 5px 10px rgba(19, 191, 166, 0.3) !important;" type="button" class="btn btn-secondary btn-icon-text mr-2 mb-2 mb-md-0">
                <i class="fe fe-printer"></i>
                Print
            </button>
            <button id="pdfbtn" type="button" class="btn btn-primary btn-icon-text mb-2 mb-md-0">
                <i class="fe fe-download-cloud "></i>
                Download Report
            </button>
        </div>
    </div>
</div>

<!-- row opened -->
<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title">Lead</div>
                <div class="card-options">
                    <a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
                    <a href="#" class="card-options-fullscreen" data-toggle="card-fullscreen"><i class="fe fe-maximize"></i></a>
                    <a href="#" class="card-options-remove" data-toggle="card-remove"><i class="fe fe-x"></i></a>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive ">
                    <table id="myDataTable" class="table table-striped table-bordered text-nowrap">
                        <thead>
                            <tr>
                                <th class="wd-15p border-bottom-0">First name</th>
                                <th class="wd-15p border-bottom-0">Last name</th>
                                <th class="wd-20p border-bottom-0">Phone Number</th>
                                <th class="wd-15p border-bottom-0">Email</th>
                                <th class="wd-10p border-bottom-0">Studio Name</th>
                                <th class="wd-25p border-bottom-0">Date Submission</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- table-wrapper -->
        </div>
        <!-- section-wrapper -->
    </div>
</div>
<!-- row closed -->
@endsection
@section('title')
Lead Listing
@endsection
@push('scripts')
<script>
    $(document).ready(function() {
        // alert('HELLO');
        $('#myDataTable').DataTable({
            processing: true,
            serverSide: true,
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            ajax: '{{ route('leadData') }}',
            columns: [
                {data: 'first_name', name: 'first_name'},
                {data: 'last_name', name: 'last_name'},
                {data: 'number', name: 'number'},
                {data: 'email', name: 'email'},
                {data: 'studio_id', name: 'studio_id'},
                {data: 'created_at', name: 'created_at'},
        ]
        });
    });
</script>
<script type="text/javascript">
    $('#exportbtn').on('click', function() {
        $('.buttons-excel').trigger( "click" );
    });
    $('#printbtn').on('click', function() {
        $('.buttons-print').trigger( "click" );
    });
    $('#pdfbtn').on('click', function() {
        $('.buttons-pdf').trigger( "click" );
    });
</script>
@endpush