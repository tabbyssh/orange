@extends('layouts.dashboardLayout')

@section('content')


<div style="margin: 142px 0px 1.5rem;">
    
</div>

<div class="row ">
    <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
        <div class="card overflow-hidden">
            <div class="card-body">
                <div class="d-flex">
                    <div class="">
                        <p class="mb-2 h6">Total Submission</p>
                        <h2 class="mb-1 ">{{ count($leads) }}</h2>
                        {{-- <p class="mb-0 text-muted"><span class="text-success">(+0.35%)<i class="fe fe-arrow-up text-success"></i></span>Increase</p> --}}
                    </div>
                    <div class=" my-auto ml-auto">
                        <div class="chart-wrapper text-center">
                            <img src="{{ asset('images/media/charticon.png') }}">
                            {{-- <canvas id="areaChart1" class="areaChart2 chartjs-render-monitor chart-dropshadow-primary overflow-hidden mx-auto"></canvas> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
        <div class="card overflow-hidden">
            <div class="card-body">
                <div class="d-flex">
                    <div class="">
                        <p class="mb-2 h6"><i class="fa fa-map-marker"></i> {{ $studiolead->studio->name }}</p>
                        <h2 class="mb-1 ">{{ $studioleadsTotal }}</h2>
                        <p class="mb-0"><span>Most Submission</span></p>
                    </div>
                    <div class=" my-auto ml-auto">
                        <div class="chart-wrapper">
                            <img src="{{ asset('images/media/charticon2.png') }}">
                            {{-- <canvas id="areaChart2" class="areaChart2 chartjs-render-monitor chart-dropshadow-secondary overflow-hidden"></canvas> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- row closed -->

<!-- row opened -->
<div class="row">
    <div class="col-lg-8 col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Single line chart</h3>
                <div class="card-options">
                    <a href="#" class="card-options-fullscreen" data-toggle="card-fullscreen"><i class="fe fe-maximize"></i></a>
                </div>
            </div>
            <div class="card-body">
                <div id="echart7" class="chartsh h-300"></div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Studios</h3>
                <div class="card-options">
                    <a href="#" class="card-options-fullscreen" data-toggle="card-fullscreen"><i class="fe fe-maximize"></i></a>
                </div>
            </div>
            <div class="">
                <div class="table-responsive">
                    <table class="dstable table card-table border table-bordered text-nowrap">  
                        <tbody>
                            @if(count($studios) > 0)
                                @foreach($studios as $studio)
                                <tr>
                                    <td>{{ $studio->name }}</th>
                                    <td>{{ $studio->id}}</td>
                                </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- row closed -->

<!-- row opened -->
<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title">Latest Lead Form</div>
                <div class="card-options">
                    <a style="color: #09B0EC;font-size: 15px;text-decoration: underline;" href="{{ route('lead.index') }}">View All</a>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive ">
                    <table class="studioListingTable table table-bordered border-top table-hover mb-0 text-nowrap">
                            <thead>
                                <tr>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Phone</th>
                                    <th>Email Address</th>
                                    <th>Studio Name</th>
                                    <th>Date Submission</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($leads) > 0)
                                    @foreach($leads as $lead)
                                    <tr>
                                        <td>{{ $lead->first_name }}</th>
                                        <td>{{ $lead->last_name}}</td>
                                        <td>{{ $lead->number }}</td>
                                        <td>{{ $lead->email }}</td>
                                        <td>{{ $lead->studio->name }}</td>
                                        <td>{{ $lead->created_at }}</td>
                                    </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                </div>
            </div>
            <!-- table-wrapper -->
        </div>
        <!-- section-wrapper -->
    </div>
</div>
<!-- row closed -->
@endsection
@section('title')
Dashboard
@endsection
